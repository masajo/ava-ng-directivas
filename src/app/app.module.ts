import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AttrDirective } from './directives/attr.directive';
import { StructDirective } from './directives/struct.directive';
import { LifecycleDirective } from './directives/lifecycle.directive';
import { EjemploAttrComponent } from './components/ejemplo-attr/ejemplo-attr.component';
import { EjemploStructComponent } from './components/ejemplo-struct/ejemplo-struct.component';
import { EspiadoComponent } from './components/espiado/espiado.component';

@NgModule({
  declarations: [
    AppComponent,
    AttrDirective,
    StructDirective,
    LifecycleDirective,
    EjemploAttrComponent,
    EjemploStructComponent,
    EspiadoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
