import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appLifecycle]'
})
export class LifecycleDirective implements OnInit, OnDestroy{

  constructor() { }

  ngOnInit(): void {
    this.cicloDeVida('ngOnInit');
  }

  ngOnDestroy(): void {
    this.cicloDeVida('ngOnDestroy');
  }

  cicloDeVida(hook: string) {
    console.log(`CICLO DE VIDA: ${hook}`)
  }

}
